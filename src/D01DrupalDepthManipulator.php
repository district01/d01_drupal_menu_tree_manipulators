<?php

namespace Drupal\d01_drupal_menu_tree_manipulators;

/**
 * Class BamMenuLinkTreeManipulator.
 *
 * @package Drupal\bam_menu
 */
class D01DrupalDepthManipulator implements D01DrupalDepthManipulatorInterface {

  /**
   * {@inheritdoc}
   */
  public function filterDepth(array $tree, $depth = FALSE) {
    if (!$depth) {
      return $tree;
    }

    foreach ($tree as $index => $item) {
      if ($item->depth >= $depth) {
        $tree[$index]->subtree = [];
        $tree[$index]->hasChildren = FALSE;
      }
      else {
        $this->filterDepth($item->subtree, $depth);
      }
    }
    return $tree;
  }

}
