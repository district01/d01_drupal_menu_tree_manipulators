<?php

namespace Drupal\d01_drupal_menu_tree_manipulators;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;

/**
 * Class D01DrupalLocaleEntityManipulator.
 *
 * @package Drupal\bam_menu
 */
class D01DrupalLocaleEntityManipulator implements D01DrupalLocaleEntityManipulatorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Load entity from provided menu link.
   *
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface|null
   *   Boolean if menu link has no metadata. NULL if entity not found and
   *   an EntityInterface if found.
   */
  private function getEntity(MenuLinkContent $link) {
    // MenuLinkContent::getEntity() has protected visibility and cannot be used
    // to directly fetch the entity.
    $metadata = $link->getMetaData();

    if (empty($metadata['entity_id'])) {
      return FALSE;
    }
    return $this->entityTypeManager
      ->getStorage('menu_link_content')
      ->load($metadata['entity_id']);
  }

  /**
   * {@inheritdoc}
   */
  public function filterLocaleEntity(array $tree, $lang_code = FALSE) {

    // Get the language.
    $current_language = $this->languageManager->getCurrentLanguage();
    $language = $lang_code ? $lang_code : $current_language->getId();

    // Loop over menu tree.
    foreach ($tree as $index => $item) {

      // When item has children handle children first.
      if ($item->hasChildren) {
        $item->subtree = $this->filterLocaleEntity($item->subtree, $language);

        // Since we unset untranslated children we must
        // validate if subtree still has children.
        if (count($item->subtree) === 0) {
          $item->hasChildren = FALSE;
        }
      }

      // Get the link item to check.
      $link = $item->link;

      // Make sure we pass an instance of MenuLinkContent plugin
      // so we can load the MenuLinkContent entity from it's metadata.
      if ($link instanceof MenuLinkContent) {

        /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $entity */
        $entity = $this->getEntity($link);

        if (!$entity) {
          // Skip items that aren't entities or where
          // the entity could not be loaded.
          continue;
        }

        if (!$entity->isTranslatable()) {
          // Skip untranslatable items.
          continue;
        }

        if (!$entity->hasTranslation($language)) {
          // Unset items that don't have a translation in the passed language.
          unset($tree[$index]);
          continue;
        }
      }
    }
    return $tree;
  }

}
