<?php

namespace Drupal\d01_drupal_menu_tree_manipulators;

/**
 * Class D01DrupalDepthManipulatorInterface.
 *
 * @package Drupal\d01_drupal_menu_tree_manipulators
 */
interface D01DrupalDepthManipulatorInterface {

  /**
   * Filter out links that are too deep in the tree.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The menu link tree to manipulate.
   * @param bool|int $depth
   *   The depth of the menu items.
   *   When no number is given it will show all items.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement
   *   The manipulated menu link tree.
   */
  public function filterDepth(array $tree, $depth = 1);

}
