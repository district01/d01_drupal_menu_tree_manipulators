<?php

namespace Drupal\d01_drupal_menu_tree_manipulators;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class D01DrupalLocaleEntityManipulatorInterface.
 *
 * @package Drupal\d01_drupal_menu_tree_manipulators
 */
interface D01DrupalLocaleEntityManipulatorInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager);

  /**
   * Filter out links that are not translated to the passed language.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The menu link tree to manipulate.
   * @param bool|string $lang_code
   *   The language code to filter on.
   *   when empty we use the current language.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement
   *   The manipulated menu link tree.
   */
  public function filterLocaleEntity(array $tree, $lang_code = FALSE);

}
